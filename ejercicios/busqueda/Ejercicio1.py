# coding=utf-8

# Ejercicio 1: Agenda simplificada
# Escribir una función que reciba una cadena a buscar y una lista de
# tuplas (nombre_completo, telefono) y busque dentro de la lista todas
# las entradas que contengan en el nombre completo la cadena recibida
# (puede ser el nombre, el apellido o sólo una parte de cualquiera de
# ellos). Debe devolver una lista con todas las tuplas encontradas.


def ejercicio1(cadena, tuplas):
    rval = []
    for tupla in tuplas:
        if cadena in tupla[0]:
            rval.append(tupla)

    return rval
