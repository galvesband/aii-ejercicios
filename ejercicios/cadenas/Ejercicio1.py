# coding=utf-8

# Texto del ejercicio:
# Escribir funciones que dada una cadena y un caracter:
from __future__ import print_function

import string


class Ejercicio1:
    def __init__(self):
        pass

    # a) Inserte el caracter entre cada letra de la cadena.
    @staticmethod
    def ejercicio1a(cadena, caracter):
        return caracter.join(list(cadena))

    # b) Reemplace todos los espacios por el caracter.
    @staticmethod
    def ejercicio1b(cadena, caracter):
        return cadena.replace('_', caracter)

    # c) Reemplace todos los dígitos en la cadena por el caracter.
    @staticmethod
    def ejercicio1c(cadena, caracter):
        translation_table = string.maketrans('0123456789', caracter * 10)
        return cadena.translate(translation_table)

    # d) Inserte el caracter cada 3 digitos en la cadena.
    @staticmethod
    def ejercicio1d(cadena, caracter):
        rval = []
        for i in range(0, len(cadena), 3):
            if i+3 < len(cadena):
                rval.append(cadena[i:i+3])
                rval.append(caracter)
            else:
                rval.append(cadena[i:])
        return "".join(rval)
