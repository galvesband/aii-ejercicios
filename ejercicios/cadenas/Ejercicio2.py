# coding=utf-8

# Texto del ejercicio:
# Escribir funciones que dadas dos cadenas de caracteres:
import string


class Ejercicio2:
    def __init__(self):
        pass

    # Indique si la segunda cadena es una subcadena de la primera
    @staticmethod
    def ejercicio2a(subcadena, cadena):
        return cadena.find(subcadena) != -1

    # Devuelva la que sea anterior en orden alfabético.
    @staticmethod
    def ejercicio2b(cadena1, cadena2):
        return cadena1 if cadena1.lower() < cadena2.lower() else cadena2
