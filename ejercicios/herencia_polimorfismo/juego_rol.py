# coding=utf-8


class Personaje:
    def __init__(self, vida, posicion, velocidad):
        self.vida = vida
        self.posicion = posicion
        self.velocidad = velocidad

    def recibir_ataque(self, damage):
        self.vida -= damage
        if self.vida < 0:
            print("Pesonaje con vida por debajo de 0!")

    def mover(self, direction):
        if direction == 'up':
            self.posicion[0] += self.velocidad
        elif direction == 'down':
            self.posicion[0] -= self.velocidad
        elif direction == 'left':
            self.posicion[1] -= self.velocidad
        elif direction == 'right':
            self.posicion[1] += self.velocidad


class Soldado(Personaje):
    def __init__(self, ataque, vida, posicion, velocidad):
        Personaje.__init__(self, vida, posicion, velocidad)
        self.ataque = ataque

    def atacar(self, personaje):
        assert isinstance(personaje, Personaje)
        personaje.recibir_ataque(self.ataque)


class Campesino(Personaje):
    def __init__(self, cosecha, vida, posicion, velocidad):
        Personaje.__init__(self, vida, posicion, velocidad)
        self.cosecha = cosecha

    def cosechar(self):
        return self.cosecha
