# coding=utf-8


from ejercicios.objetos.Corcho import Corcho


class Botella:
    def __init__(self, corcho):
        assert isinstance(corcho, Corcho)
        self.corcho = corcho
