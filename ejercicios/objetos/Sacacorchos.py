# coding=utf-8


from Botella import Botella


class Sacacorchos:
    def __init__(self):
        self.corcho = None

    def destapar(self, botella):
        assert isinstance(botella, Botella)
        if botella.corcho is not None:
            self.corcho = botella.corcho
            botella.corcho = None

    def limpiar(self):
        rval = self.corcho
        if self.corcho is not None:
            rval = self.corcho
            self.corcho = None

        return rval
