# coding=utf-8

# Ejercicio1: Campaña electoral


from __future__ import print_function


class Ejercicio1:
    def __init__(self):
        pass

    # a) Escribir una función que reciba una tupla con nombres
    #    y para cada nombre imprima el mensaje
    #    "Estimado <nombre>, vote por mi."
    @staticmethod
    def ejercicio1a(nombres):
        for nombre in nombres:
            print("Estimado " + nombre + ", vote por mí.")

    # b) Escribir una función que reciba una tupla con nombres, una posición
    #    de origen p y una cantidad n, e imprima el mensaje anterior para los
    #    n nombres que se encuentran a partir de la posición p
    @staticmethod
    def ejercicio1b(nombres, p, n):
        for nombre in nombres[p:p+n]:
            print("Estimado " + nombre + ", vote por mí.")

    # c) Modificar las funciones anteriores para que tengan en cuenta el género
    #    del destinatario. Para ello deberán recibir una tupla de tuplas,
    #    conteniendo el nombre y el género.
    @staticmethod
    def ejercicio1c1a(nombres_y_generos):
        for nombre_y_genero in nombres_y_generos:
            print("Estimad" + ("o " if nombre_y_genero[1] == 'm' else "a ") + nombre_y_genero[0] + ", vote por mí.")

    @staticmethod
    def ejercicio1c1b(nombres_y_generos, p, n):
        for nombre_y_genero in nombres_y_generos[p:p+n]:
            print("Estimad" + ("o " if nombre_y_genero[1] == 'm' else "a ") + nombre_y_genero[0] + ", vote por mí.")
