# coding=utf-8

# Ejercicio2
# Escribir una función que reciba una lista de tuplas (Apellido, Nombre,
# Inicial_segundo_nombre) y devuelva una lista de cadenas donde cada una
# contenga primero el nombre, luego la inicial con un punto y luego
# el apellido.


def ejercicio2(lista_de_tuplas):
    rval = []
    for tupla in lista_de_tuplas:
        rval.append(tupla[0] + " " + tupla[1] + ". " + tupla[2])

    return rval

