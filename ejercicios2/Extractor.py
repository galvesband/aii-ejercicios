# encoding=utf-8

import re
import urllib2

from Modelo import Modelo


def extraer_datos_cadena(modelo, cadena):
    assert isinstance(modelo, Modelo)

    regexp = r"<item>\s*<title>(.*)</title>\s*<link>(.*)</link>\s*<description>.*</description>\s*"
    regexp += r"<author>.*</author>\s*(<category>.*</category>)?\s*<guid.*</guid>\s*<pubDate>(.*)</pubDate>\s*</item>"
    results = re.findall(regexp, cadena)
    results_procesado = []
    for record in results:
        results_procesado.append((record[0], record[1], record[3]))

    modelo.insert_list_of_records(results_procesado)


def extraer_datos_rss(modelo, url):
    reader = urllib2.urlopen(url)
    extraer_datos_cadena(modelo, reader.read())
