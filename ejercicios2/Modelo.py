# encoding=utf-8

import sqlite3


class Modelo:
    def __init__(self, path_to_db):
        self.path_to_db = path_to_db
        self.connection = sqlite3.connect(self.path_to_db)
        # Para evitar problemas con el conjunto de caracteres que maneja la BD
        self.connection.text_factory = str

    def __delete__(self, instance):
        if isinstance(self.connection, sqlite3.Connection):
            self.connection.close()

    def create_database(self):
        # Crear modelo de datos, eliminar el anterior si existe
        self.connection.execute('''
            DROP TABLE IF EXISTS NOTICIAS;
        ''')
        self.connection.execute('''
            CREATE TABLE NOTICIAS (
              ID INTEGER PRIMARY KEY AUTOINCREMENT,
              TITULO            TEXT NOT NULL,
              LINK              TEXT NOT NULL,
              FECHA             TEXT NOT NULL
            );
        ''')

    def insert_new_record(self, titulo, link, fecha):
        self.connection.execute('''
            INSERT INTO NOTICIAS (TITULO, LINK, FECHA)
            VALUES (?, ?, ?);
        ''', (titulo, link, fecha))

    def insert_list_of_records(self, lista):
        for record in lista:
            self.insert_new_record(record[0], record[1], record[2])

    def get_all(self):
        rval = []
        cursor = self.connection.execute('''
            SELECT TITULO, LINK, FECHA FROM NOTICIAS;
        ''')
        for record in cursor:
            rval.append((record[0], record[1], record[2]))

        return rval

    def get_by_fecha(self, fecha):
        cursor = self.connection.execute('''
            SELECT TITULO, LINK, FECHA FROM NOTICIAS
            WHERE FECHA LIKE ?
        ''', ("%" + fecha + "%",))
        rval = []
        for record in cursor:
            print("Encontrado: " + record[0] + " " + record[1] + " " + record[2])
            rval.append((record[0], record[1], record[2]))

        return rval
