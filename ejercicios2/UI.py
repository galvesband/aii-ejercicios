# encoding=utf-8
from Tkinter import *
from Modelo import Modelo
from Extractor import extraer_datos_rss


def ventana_principal():
    def extraer_datos_tk():
        extraer_datos_rss(modelo, "http://www.us.es/rss/feed/portada")

    def listar_bd_tk():
        mostrar_etiqueta(modelo.get_all())

    def buscar_bd_tk():
        def listar_busqueda(event):
            print("Buscando por " + en.get())
            resultados = modelo.get_by_fecha(en.get())
            print("Encontrados " + str(len(resultados)) + " resultados. Mostrando...")
            mostrar_etiqueta(resultados)
            print("Done")

        v = Toplevel()
        lb = Label(v, text="Introduzca el mes (Xxx): ")
        lb.pack(side=LEFT)
        en = Entry(v)
        en.bind("<Return>", listar_busqueda)
        en.pack(side=LEFT)

    modelo = Modelo('ejercicios2.db')
    modelo.create_database()

    top = Tk()
    almacenar = Button(top, text="Almacenar", command=extraer_datos_tk)
    almacenar.pack(side=LEFT)

    listar = Button(top, text="Listar", command=listar_bd_tk)
    listar.pack(side=LEFT)

    buscar = Button(top, text="Buscar", command=buscar_bd_tk)
    buscar.pack(side=LEFT)

    top.mainloop()


def mostrar_etiqueta(lista):
    v = Toplevel()
    sc = Scrollbar(v)
    sc.pack(side=RIGHT, fill=Y)
    lb = Listbox(v, width=150, yscrollcommand=sc.get)
    for elemento in lista:
        lb.insert(END, elemento[0])
        lb.insert(END, elemento[1])
        lb.insert(END, elemento[2])
        lb.insert(END, '')
    lb.pack(side=LEFT, fill=BOTH)
    sc.config(command=lb.yview)

if __name__ == "__main__":
    ventana_principal()
