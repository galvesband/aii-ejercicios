# encoding=utf-8

from whoosh.index import create_in, open_dir
from whoosh.fields import *
from whoosh.qparser import QueryParser
from Tkinter import *
import os.path


# Devuelve el índice creado con el esquema
# Si el directorio existe -> carga el índice directamente de ahí y devuelve el índice y True
# Si no existe -> lo crea y crea en él un nuevo índice vacío y False
def load_index(dir_name):
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
        # Crear schema e índice
        schema = Schema(
            email_from=ID(stored=True),
            email_to=ID(stored=True),
            subject=TEXT(stored=True),
            body=TEXT
        )
        return create_in(dir_name, schema), True
    else:
        return open_dir(dir_name), False


def load_emails(email_dir_name, empty_index):
    if not os.path.exists(email_dir_name):
        return False

    emails = []
    for dir_entry in os.listdir(email_dir_name):
        file_path = email_dir_name + "/" + dir_entry
        if os.path.isfile(file_path):
            emails.append(parse_email_file(file_path))

    writer = empty_index.writer()
    print("Leídos " + str(len(emails)) + " emails. Indexando...")
    for email in emails:
        writer.add_document(
            email_from=email[0],
            email_to=email[1],
            subject=email[2],
            body=email[3]
        )
    writer.commit()


def parse_email_file(file_path):
    if not os.path.exists(file_path) or not os.path.isfile(file_path):
        return None

    my_file = open(file_path, "r")
    # Primera línea es email del remitente
    # Segunda línea es emails de destinatarios separados por espacios
    # Tercera línea es asunto
    # El resto es el cuerpo del correo
    rval = [
        unicode(my_file.readline().replace("\r\n", ""), "utf-8"),
        unicode(my_file.readline().replace("\r\n", ""), "utf-8"),
        unicode(my_file.readline().replace("\r\n", ""), "utf-8"),
        unicode(my_file.read().replace("\r\n", ""), "utf-8")
    ]

    return rval


def main_ui():
    def on_remitente(event):
        print(u"Buscando '" + remitente_entry.get() + u"' en el campo remitente...")
        # Realizar búsqueda en el campo de remitentes (from)
        with ix.searcher() as searcher:
            query = QueryParser("email_from", ix.schema).parse(remitente_entry.get())
            results = searcher.search(query)

            # Crear ventana de resultados
            resultado_ventana = Toplevel()
            info_lbl = Label(resultado_ventana, text="Encontrados " + str(len(results)) + " resultados.")
            info_lbl.pack(side=TOP)
            resultados_lst = Listbox(resultado_ventana)
            resultados_lst.config(width=100)
            resultados_lst.pack(side=TOP)

            print(u"Encontrados " + unicode(str(len(results)), "utf-8") + u" resultados.")
            for result in results:
                resultados_lst.insert(END, "from: " + result["email_from"])
                resultados_lst.insert(END, "to: " + result["email_to"])
                resultados_lst.insert(END, "subject: " + result["subject"])
                resultados_lst.insert(END, "")

    top = Tk()
    remitente_entry = Entry(top)
    remitente_entry.bind("<Return>", on_remitente)
    remitente_entry.pack(side=LEFT)

    top.mainloop()


if __name__ == "__main__":
    ix, es_nuevo = load_index("index")
    if es_nuevo:
        print(u"Inicializando índice...")
        load_emails("emails", ix)
    else:
        print(u"Índice cargado.")

    main_ui()
