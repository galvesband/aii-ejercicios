# encoding=utf-8

from Tkinter import *
import sqlite3
import urllib2
import re
from bs4 import BeautifulSoup


class Modelo:
    def __init__(self, path_to_db):
        self.path_to_db = path_to_db
        self.connection = sqlite3.connect(self.path_to_db)
        self.connection.text_factory = str

    def create_database(self):
        # Crear modelo de datos, eliminar el anterior si existe
        self.connection.execute('''
            DROP TABLE IF EXISTS RESULTADOS;
        ''')
        self.connection.execute('''
            DROP TABLE IF EXISTS GOLES;
        ''')
        self.connection.execute('''
            CREATE TABLE RESULTADOS (
                ID INTEGER PRIMARY KEY AUTOINCREMENT,
                JORNADA           TINYINT NOT NULL,
                LINK              TEXT NOT NULL,
                EQUIPO1           TEXT NOT NULL,
                EQUIPO2           TEXT NOT NULL,
                RESULTADO         TEXT NOT NULL
            );
        ''')
        self.connection.execute('''
            CREATE TABLE GOLES (
                ID INTEGER PRIMARY KEY AUTOINCREMENT,
                JORNADA INTEGER NOT NULL,
                EQUIPO TEXT NOT NULL,
                JUGADOR TEXT NOT NULL,
                MINUTO INTEGER NOT NULL
            );
        ''')
        self.connection.commit()

    def insert_resultados(self, lista_resultados):
        for resultado in lista_resultados:
            self.connection.execute('''
                INSERT INTO RESULTADOS (JORNADA, LINK, EQUIPO1, EQUIPO2, RESULTADO)
                VALUES (?, ?, ?, ?, ?);
            ''', resultado)
        self.connection.commit()

    def insert_goles(self, lista_goles):
        for gol in lista_goles:
            self.connection.execute('''
                INSERT INTO GOLES (JORNADA, EQUIPO, JUGADOR, MINUTO)
                VALUES (?, ?, ?, ?)
            ''', gol)
        self.connection.commit()

    def get_jornadas(self):
        rset = self.connection.execute('''
            SELECT DISTINCT(JORNADA) FROM RESULTADOS;
        ''')

        return rset

    def get_partidos_jornada(self, jornada):
        rset = self.connection.execute('''
            SELECT EQUIPO1, EQUIPO2, LINK FROM RESULTADOS
            WHERE JORNADA = ?
        ''', (jornada,))

        return rset

    def get_goles_partido(self, jornada, equipo1, equipo2):
        rset = self.connection.execute('''
            SELECT EQUIPO, JUGADOR, MINUTO FROM GOLES
            WHERE (EQUIPO = ? OR EQUIPO = ?) AND JORNADA = ?
        ''', (equipo1, equipo2, jornada))

        return rset

    def get_max_goleador(self, equipo):
        rset = self.connection.execute('''
            SELECT COUNT(*) c, JUGADOR
            FROM GOLES
            WHERE EQUIPO LIKE ?
            ORDER BY c DESC
            LIMIT 1
        ''', ("%" + equipo + "%"))

        print(rset)
        return rset


def extraer_resultados(url):
    # Cargamos html en Beautiful Soup
    html = urllib2.urlopen(url).read()
    soup = BeautifulSoup(html, "lxml")

    # Aqui guardaremos los resultados que queremos insertar
    rset = []
    # Buscamos resultados de las jornadas listadas en la web
    # Primero localizamos las jornadas
    id_regexp = r"^jornada-([0-9])+$"
    jornadas_divs = soup.find_all("div", {"id": re.compile(id_regexp)})
    print("Encontradas " + str(len(jornadas_divs)) + " jornadas.")
    for jornada_div in jornadas_divs:
        # Determinar la jornada que estamos examinando
        title_a = jornada_div.find("a")
        jornada = int(str(title_a.string).replace("Jornada ", ""))

        # Encontrar resultados de la jornada e ir almacenandolos en rset
        partido_trs = jornada_div.find_all("tr", {"itemtype": "http://schema.org/SportsEvent"})
        # Para cada partido tenemos que determinar equipo1, equipo2, resultado y enlace a retransmisión
        for partido_tr in partido_trs:
            # Equipos
            equipos_span = partido_tr.find_all("span", "nombre-equipo")
            equipo1 = equipos_span[0].string
            equipo2 = equipos_span[1].string
            # Resultado y enlace
            resultado_a = partido_tr.find("a", "resultado")
            if resultado_a:
                enlace = resultado_a.attrs["href"]
                resultado = str(resultado_a.string).strip()

                # Solo queremos resultados de partidos terminados o en juego
                regexp = re.compile("^\s*[0-9]+ - [0-9]+\s*$")
                if regexp.match(resultado):
                    # Añadir resultado a rset
                    rset.append((jornada, enlace, equipo1, equipo2, resultado))

    # Añadir resultados a bd
    print("Encontrados " + str(len(rset)) + " resultados.")
    return rset


# Examina url, devuelve una tupla con 2 listas, una con los goles locales y otra con los visitantes
def extraer_goles(url):
    def get_goles_de_eventos(eventos):
        rset_2 = []
        for evento in eventos:
            sub_eventos = evento.find_all("div", {"class": "txt-evento"})
            for sub_evento in sub_eventos:
                es_gol = sub_evento.find("span", {"class": "as-icon-futbol"})
                if es_gol:
                    minuto_str = sub_evento.find("span", {"class": "min-evento"})
                    minuto = int(re.search("\d+", minuto_str.string).group(0))
                    jugador = sub_evento.find("strong").string
                    rset_2.append((minuto, jugador))

        return rset_2

    html = urllib2.urlopen(url).read()
    soup = BeautifulSoup(html, "lxml")

    eventos_local = soup.find("div", {"class": "eventos-local"})
    goles_local = get_goles_de_eventos(eventos_local.find_all("div", {"class": "evento"}))

    eventos_visitante = soup.find("div", {"class": "eventos-visit"})
    goles_visitante = get_goles_de_eventos(eventos_visitante.find_all("div", {"class": "evento"}))

    return goles_local, goles_visitante


def extraer_datos_e_insertar(m):
    assert isinstance(m, Modelo)
    # Crear base de datos
    m.create_database()

    # Extraer resultados de jornadas
    resultados = extraer_resultados("http://resultados.as.com/resultados/futbol/primera/2016_2017/calendario/")
    m.insert_resultados(resultados)

    # Extraer goles de jornadas
    jornadas = m.get_jornadas()
    for jornada in jornadas:
        num_jornada = jornada[0]
        partidos_jornada = m.get_partidos_jornada(num_jornada)
        for partido_jornada in partidos_jornada:
            # Extraer goles de jornada
            equipo1 = partido_jornada[0]
            equipo2 = partido_jornada[1]
            link_partido = partido_jornada[2]
            print("Extrayendo goles de partido de jornada " + str(jornada[0]) + ": " + equipo1 + " - " + equipo2)
            goles_equipo1, goles_equipo2 = extraer_goles("http://resultados.as.com" + link_partido)

            # Preparar una lista con los goles con los campos tal y como los espera el modelo
            rset = []
            for gol in goles_equipo1:
                rset.append((jornada[0], equipo1, gol[1], gol[0]))
            for gol in goles_equipo2:
                rset.append((jornada[0], equipo2, gol[1], gol[0]))

            # Guardamos los goles del partido en BD
            print("Insertando goles: " + str(len(rset)))
            m.insert_goles(rset)


def ui_ventana_principal():
    # Boton 1 - Reset a la BD y extraer datos
    def ui_extraer_datos_e_insertar():
        modelo.create_database()
        extraer_datos_e_insertar(modelo)

    # Boton 2 - Mostrar ventana de selección de jornada,
    # al seleccionar jornada mostrar ventana con partidos y goles por partido
    def ui_seleccion_jornada():
        # Muestra ventana con partidos de jornada seleccionada en jornada_lst
        def ui_on_jornada_selected(evento):
            # Al seleccionar un partido, rellena la lista de goles
            def ui_on_partido_selected():
                texto_partido = partidos_spin.get()
                print("Obteniendo goles de " + texto_partido + "...")

                regexp = re.compile("^(.*) - (.*)$")
                matches = regexp.match(texto_partido)
                if matches:
                    equipo1 = matches.group(1)
                    equipo2 = matches.group(2)

                    # Limpiar anterior lista de goles
                    goles_lst.delete(0, END)
                    # Buscar goles del partido
                    i = 0
                    for gol in modelo.get_goles_partido(jornada_actual, equipo1, equipo2):
                        goles_lst.insert(i, gol[1] + " del " + gol[0] + " ha marcado en el minuto " + str(gol[2]))

            if (len(jornada_lst.curselection())) > 0:
                jornada_actual = jornada_lst.curselection()[0] + 1
                ventana_goles_partido = Toplevel()
                partido_lbl = Label(ventana_goles_partido, text="Seleccione un partido de la jornada " + str(jornada_actual))
                partido_lbl.pack(side=TOP)

                partidos = []
                for partido in modelo.get_partidos_jornada(jornada_actual):
                    partidos.append(partido[0] + " - " + partido[1])
                partidos_spin = Spinbox(ventana_goles_partido, values=partidos, command=ui_on_partido_selected)
                partidos_spin.pack(side=TOP)

                goles_lst = Listbox(ventana_goles_partido)
                goles_lst.config(width=100)
                goles_lst.pack(side=TOP)

                # Llamamos a ui_on_partido_selected para que muestre los goles del partido
                # inicial de la lista de partidos
                ui_on_partido_selected()

        ventana_seleccion_jornada = Toplevel()
        jornada_lbl = Label(ventana_seleccion_jornada, text="Seleccione una jornada:")
        jornada_lbl.pack(side=TOP)

        jornada_lst = Listbox(ventana_seleccion_jornada, selectmode="SINGLE")
        i = 0
        for jornada in modelo.get_jornadas():
            jornada_lst.insert(i, jornada[0])
            i += 1
        jornada_lst.bind("<<ListboxSelect>>", ui_on_jornada_selected)
        jornada_lst.pack(side=TOP)

    # Boton 3: mostrar ventana de selección de equipo
    #          al seleccionar equipo mostrar máximo goleador
    def ui_seleccion_equipo():
        def on_equipo_selected(event):
            print("Equipo: " + equipo_entry.get())
            print(modelo.get_max_goleador(equipo_entry.get()))

        ventana_seleccion_equipo = Toplevel()

        equipo_lbl = Label(ventana_seleccion_equipo, text="Escriba el nombre de un equipo:")
        equipo_lbl.pack(side=TOP)

        equipo_entry = Entry(ventana_seleccion_equipo)
        equipo_entry.bind("<Return>", on_equipo_selected)
        equipo_entry.pack(side=TOP)

        goleador_lbl = Label(ventana_seleccion_equipo, text="-")
        goleador_lbl.pack(side=TOP)

    modelo = Modelo("p1.db")

    # Ventana principal: tres botones
    # Boton1: almacenar datos en BD
    # Boton2: mostrar ventana para selección de jornada
    #         al seleccionar: mostrar ventana de goles en partido
    # Boton3: goleadores: a partir de nombre de equipo (introducido
    #         por usuario) listar máximo goleador
    top = Tk()
    almacenar_btn = Button(top, text="Almacenar", command=ui_extraer_datos_e_insertar)
    almacenar_btn.pack(side=LEFT)

    goles_btn = Button(top, text="Goles en partido", command=ui_seleccion_jornada)
    goles_btn.pack(side=LEFT)

    goleadores_btn = Button(top, text="Goleadores", command=ui_seleccion_equipo)
    goleadores_btn.pack(side=LEFT)

    top.mainloop()


if __name__ == "__main__":
    ui_ventana_principal()
