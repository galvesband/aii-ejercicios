# encoding=utf-8
from Tkinter import *
import sqlite3
import urllib2
import re
from bs4 import BeautifulSoup


class Modelo:
    def __init__(self, path_to_db):
        self.path_to_db = path_to_db
        self.connection = sqlite3.connect(self.path_to_db)
        self.connection.text_factory = str

    def create_database(self):
        # Crear modelo de datos, eliminar el anterior si existe
        self.connection.execute('''
                    DROP TABLE IF EXISTS HILOS_FORO;
                ''')
        self.connection.execute('''
                    CREATE TABLE HILOS_FORO (
                        ID INTEGER PRIMARY KEY AUTOINCREMENT,
                        TITULO            TEXT NOT NULL,
                        LINK              TEXT NOT NULL,
                        AUTOR             TEXT NOT NULL,
                        FECHA             TEXT NOT NULL,
                        N_RESPUESTAS      INTEGER NOT NULL,
                        N_VISITAS         INTEGER NOT NULL
                    );
                ''')
        self.connection.commit()

    def insert_resultados(self, lista_resultados):
        for resultado in lista_resultados:
            self.connection.execute('''
                INSERT INTO HILOS_FORO (TITULO, LINK, AUTOR, FECHA, N_RESPUESTAS, N_VISITAS)
                VALUES (?, ?, ?, ?, ?, ?);
            ''', resultado)
        self.connection.commit()
        print("Insertados en modelo " + str(len(lista_resultados)) + " resultados.")

    def search_thread(self, title_str):
        cursor = self.connection.execute('''
            SELECT TITULO, LINK, AUTOR, FECHA, N_RESPUESTAS, N_VISITAS
            FROM HILOS_FORO
            WHERE TITULO LIKE ?
        ''', ("%" + title_str + "%",))
        rset = []
        for record in cursor:
            rset.append((record[0], record[1], record[2], record[3], record[4], record[5]))
        return rset


def extraer_datos_hilos(url):
    html = urllib2.urlopen(url).read()
    soup = BeautifulSoup(html, "lxml")

    # Guardamos aquí resultados
    rset = []

    hilos_lis = soup.findAll("li", {"class": "threadbit"})
    for hilo_li in hilos_lis:
        # De cada hilo queremos el título, el enlace, el autor, la fecha y el número de respuestas y visitas
        # Primero el título y el enlace
        title_a = hilo_li.find("a", {"class": "title"})
        titulo = title_a.string
        link = title_a.attrs["href"]

        # Ahora el autor y la fecha
        autor = hilo_li.find("a", {"class": "username"}).string
        fecha_posible_dds = hilo_li.findAll("dd")
        fecha = False
        for posible_fecha_dd in fecha_posible_dds:
            if posible_fecha_dd.find("span", {"class": "time"}):
                fecha = str(posible_fecha_dd.contents[0]).replace(",", "")
                break
        if not fecha:
            fecha = "<fecha desconocida>"

        # Número de respuestas y visitas
        estadisticas_hilo_ul = hilo_li.find("ul", {"class": "threadstats"})
        respuestas, visitas = 0, 0
        for child in estadisticas_hilo_ul.children:
            if child.name == "li" and "class" not in child.attrs:
                li = child

                if li.find("a", {"class": "understate"}):
                    respuestas = int(str(li.find("a", {"class": "understate"}).string).replace(",", ""))

                results = re.findall(r"^Visitas:\s+((\d|,)+)$", str(li.string).strip())
                if len(results) > 0:
                    visitas = int(str(results[0][0]).replace(',', ''))

        # Crear tupla con resultados y añadirla a la lista de resultados
        rset.append((titulo, link, autor, fecha, respuestas, visitas))

    return rset


def ui_ventan_principal():
    def ui_extraer_datos_e_insertar():
        modelo.create_database()
        hilos = extraer_datos_hilos("http://foros.derecho.com/foro/20-Derecho-Civil-General")
        modelo.insert_resultados(hilos)

    def ui_buscar(evnt):
        print("Buscando cadena en títulos: " + buscar_entry.get())
        results = modelo.search_thread(buscar_entry.get())
        print("Encontrados " + str(len(results)) + " hilos.")

        # Ahora hay que mostrar los resultados en una ventana
        ui_ventana_resultados = Toplevel()

        info2_lbl = Label(ui_ventana_resultados, text="Resultados de la búsqueda: '" + buscar_entry.get() + "'")
        info2_lbl.pack(side=TOP)

        scrollbar = Scrollbar(ui_ventana_resultados)
        scrollbar.pack(side=RIGHT, fill=Y)
        resultados_lst = Listbox(ui_ventana_resultados, width=150, yscrollcommand=scrollbar.get)
        resultados_lst.pack(side=TOP)

        for result in results:
            resultados_lst.insert(END, result[0])
            resultados_lst.insert(END, result[1])
            resultados_lst.insert(END, "Escrito por " + result[2] + " el " + result[3])
            resultados_lst.insert(END, "Tiene " + result[4] + " respuestas y " + result[5] + " visitas.")
            resultados_lst.insert(END, '')

    modelo = Modelo("p1_extra.db")

    # Ventana principal: 1 botón y una caja de texto
    # Boton1: almacenar datos en BD
    # Caja de texto: permite buscar en los títulos de los hilos del modelo
    top = Tk()

    info_lbl = Label(top, text="Para buscar introduce un texto y pulsa <Enter>")
    info_lbl.pack(side=TOP)

    almacenar_btn = Button(top, text="Almacenar", command=ui_extraer_datos_e_insertar)
    almacenar_btn.pack(side=LEFT)

    buscar_entry = Entry(top)
    buscar_entry.bind("<Return>", ui_buscar)
    buscar_entry.pack(side=LEFT)

    top.mainloop()


if __name__ == "__main__":
    ui_ventan_principal()
