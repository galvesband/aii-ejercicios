# encoding=utf-8

import urllib
import urllib2
from bs4 import BeautifulSoup
import re
from whoosh.index import create_in, open_dir
from whoosh.fields import *
from whoosh.qparser import QueryParser
from whoosh import query
import os.path
import datetime


class BuscadorTemasDerechoCivil:
    def __init__(self, path_to_index):
        if not os.path.exists(path_to_index):
            os.mkdir(path_to_index)
            # Crear schema e índice
            schema = Schema(
                title=TEXT(stored=True),
                link=ID(stored=True),
                author=TEXT(stored=True),
                answer_number=NUMERIC(stored=True),
                visit_number=NUMERIC(stored=True, sortable=True),
                timestamp=DATETIME(stored=True),
                content=TEXT  # No requiere almacenado
            )
            self.index = create_in(path_to_index, schema)
            self.index_empty = True
        else:
            self.index = open_dir(path_to_index)
            self.index_empty = False

    def is_empty(self):
        return self.index_empty

    def get_index(self):
        return self.index


class BuscadorRespuestasDerechoCivil:
    def __init__(self, path_to_index):
        if not os.path.exists(path_to_index):
            os.mkdir(path_to_index)
            # Crear schema e índice
            schema = Schema(
                thread_link=ID(stored=True),
                author=TEXT(stored=True),
                timestamp=DATETIME(stored=True),
                content=TEXT(stored=True)  # Este sí requiere almacenado porque hay que recuperarlo
                                           # en una de las opciones del programa.
            )
            self.index = create_in(path_to_index, schema)
            self.index_empty = True
        else:
            self.index = open_dir(path_to_index)
            self.index_empty = False

    def is_empty(self):
        return self.index_empty

    def get_index(self):
        return self.index


def extraer_datos(index_temas, index_respuestas):
    def extraer_num_paginas(soup):
        # Obtenemos el número de páginas del foro
        pagination_form = soup.find("form", {"class": "pagination"})
        paginas_a = pagination_form.find("a", {"href": "javascript://", "class": "popupctrl"})
        return int(paginas_a.string.replace(u"Página 1 de ", u""))

    def extraer_temas_y_respuestas(soup):
        writer_temas = index_temas.get_index().writer()
        writer_respuestas = index_respuestas.get_index().writer()

        hilos_lis = soup.findAll("li", {"class": "threadbit"})
        for hilo_li in hilos_lis:
            # De cada hilo queremos el título, el enlace, el autor, la fecha,
            # el número de respuestas y visitas y el contenido

            # Primero el título y el enlace
            title_a = hilo_li.find("a", {"class": "title"})
            titulo = title_a.string
            link = title_a.attrs["href"]

            # Ahora el autor y la fecha
            autor = hilo_li.find("a", {"class": "username"}).string
            fecha_posible_dds = hilo_li.findAll("dd")
            fecha = False
            for posible_fecha_dd in fecha_posible_dds:
                if posible_fecha_dd.find("span", {"class": "time"}):
                    fecha = str(posible_fecha_dd.contents[0]).replace(",", "")
                    break
            if not fecha:
                fecha = datetime.datetime.now()

            # Número de respuestas y visitas
            estadisticas_hilo_ul = hilo_li.find("ul", {"class": "threadstats"})
            respuestas, visitas = 0, 0
            for child in estadisticas_hilo_ul.children:
                if child.name == "li" and "class" not in child.attrs:
                    li = child

                    if li.find("a", {"class": "understate"}):
                        respuestas = int(str(li.find("a", {"class": "understate"}).string).replace(",", ""))

                    results = re.findall(r"^Visitas:\s+((\d|,)+)$", str(li.string).strip())
                    if len(results) > 0:
                        visitas = int(str(results[0][0]).replace(',', ''))

            # Necesitamos el contenido y parsear las respuestas al tema
            url_detalle_tema = ("http://foros.derecho.com/" + link).encode("UTF-8")
            print("Analizando tema: " + url_detalle_tema)
            soup_detalle_tema = BeautifulSoup(urllib2.urlopen(url_detalle_tema).read(), "lxml")

            # Contenido
            contenido = extraer_contenido_tema(soup_detalle_tema)

            # Añadir al indice de temas (cuando tenga el contenido)
            writer_temas.add_document(
                title=unicode(titulo),
                link=unicode(link),
                author=unicode(autor),
                answer_number=respuestas,
                visit_number=visitas,
                timestamp=fecha,
                content=unicode(contenido)
            )

            # Extraer e indexar respuestas al tema
            extraer_respuestas(soup_detalle_tema, writer_respuestas, link)

        writer_temas.commit()
        writer_respuestas.commit()

    def extraer_contenido_tema(soup):
        posts_ol = soup.find("ol", {"id": "posts"})
        blockquote = posts_ol.find("blockquote")

        return unicode(blockquote.text)

    def extraer_respuestas(soup, writer, link):
        # Queremos indexar el enlace del tema, el autor, la fecha y el contenido
        # de cada respuesta. El enlace nos lo dan como argumento y es lo que
        # enlaza ambos índices.
        post_lis = soup.find("ol", {"class": "posts"}).find_all("li", {"class": "postcontainer"})
        primero = True
        for post_li in post_lis:
            if primero:
                # Ignoramos el primer post, que es el post original y esta
                # indexado en el indice de temas.
                primero = False
                continue
            else:
                # Nombre de usuario
                username = post_li.find("a", {"class": "username"}).find("strong").string
                # Fecha
                date_string = post_li.find("span", {"class": "date"}).text
                # Contenido
                content = post_li.find("blockquote", {"class": "postcontent"}).text

                # Escribimos en el índice
                writer.add_document(
                    thread_link=unicode(link),
                    author=unicode(username),
                    timestamp=datetime.datetime.strptime(unicode(date_string).replace(u'\xa0', u' '),
                                                         u"%d/%m/%Y, %H:%M"),
                    content=unicode(content)
                )

    url_principal_temas = "http://foros.derecho.com/foro/20-Derecho-Civil-General/page{page_number}"

    # Descargamos la primera página del foro
    print("Analizando primera página del foro: " + url_principal_temas)
    html_pagina_1 = urllib2.urlopen(url_principal_temas.replace("{page_number}", "1")).read()
    soup_pagina_1 = BeautifulSoup(html_pagina_1, "lxml")
    # Obtenemos el número de páginas del foro
    num_paginas = extraer_num_paginas(soup_pagina_1)
    # FIXME Limitar artificialmente número de páginas parseadas
    num_paginas = 1
    print(u"Número de páginas del foro: " + str(num_paginas))

    # Extraemos temas y respuestas de la página
    extraer_temas_y_respuestas(soup_pagina_1)

    # Extraer temas y respuestas de las otras páginas
    if num_paginas > 1:
        for i in range(2, num_paginas):
            # URL de página i
            url_pagina_i = url_principal_temas.replace("{page_number", str(i))
            # HTML de página i
            html_pagina_i = urllib2.urlopen(url_pagina_i).read()
            # Sopa de página i
            soup_pagina_i = BeautifulSoup(html_pagina_i, "lxml")

            extraer_temas_y_respuestas(soup_pagina_i)


def menu_principal(index_temas, index_respuestas):
    # Muestra un menu y devuelve la selección del usuario
    def muestra_menu(intro, opciones):
        def print_menu():
            print((30 * "-") + intro + (30 * "-"))
            for i in range(0, len(opciones)):
                print(str(i+1) + " - " + opciones[i])

        while True:
            print_menu()
            seleccion = input(u"Selecciona una opción (1-" + str(len(opciones)) + u"):")

            if seleccion < 1 or seleccion > len(opciones):
                print(u"Selección desconocida. Vuelva a intentarlo.")
            else:
                rval = int(seleccion)
                return rval

    def mostrar_resultados(resultados):
        print("Se han encontrado " + str(len(resultados)) + " temas.")
        print("(Mostrando detalles de un máximo de 10)")
        print("Resultados " + (30 * "-"))
        # Por defecto se devuelven 10 resultados
        for resultado in resultados:
            print(u" - Título: " + resultado["title"])
            print(u"   Escrito por " + resultado["author"] + u" en fecha " + resultado["timestamp"])
            print(u"   Enlace: " + resultado["link"])
            print(u"")

    def busca_en_titulo():
        # Buscar en el título del tema (con operadores 'or', 'and' y 'not')
        # Debe mostrar toda la información de los temas que respondan a la búsqueda
        print("Indique los términos de búsqueda en los títulos de los temas:")
        print("(Puede usar 'or', 'and' y 'not' como operadores)")
        query_string = raw_input()

        qp = QueryParser("title", schema=index_temas.get_index().schema)
        q = qp.parse(unicode(query_string))

        with index_temas.get_index().searcher() as s:
            resultados = s.search(q)
            mostrar_resultados(resultados)

    def busca_en_respuestas():
        # Buscar en el contenido de las respuestas.
        # Debe mostrar autor, fecha y contenido de la respuesta.
        # Debe mostrar autor, fecha y titulo del tema
        print("Indique los términos de búsqueda en los contenidos de las respuestas:")
        print("(Puede usar 'or', 'and' y 'not' como operadores)")
        query_string = raw_input()

        qp = QueryParser("content", schema=index_respuestas.get_index().schema)
        q = qp.parse(unicode(query_string))

        with index_respuestas.get_index().searcher() as searcher_respuestas:
            with index_temas.get_index().searcher() as searcher_temas:
                resultados = searcher_respuestas.search(q)
                print("Se han encontrado " + str(len(resultados)) + " respuestas.")
                print("(Mostrando detalles de un máximo de 10)")
                print("Resultados " + (30 * "-"))
                for resultado in resultados:
                    qp_temas = QueryParser("link", schema=index_temas.get_index().schema)
                    q_tema = qp_temas.parse(resultado["thread_link"])
                    tema = searcher_temas.search(q_tema)

                    print(u" - Respuesta escrita por " + resultado["author"] + u" en fecha " + str(resultado["timestamp"]))
                    print(u"   Contenido: " + resultado["content"])
                    print(u"   Título del tema: " + tema[0]["title"])
                    print(u"   Tema escrito por " + tema[0]["author"] + u" en fecha " + str(tema[0]["timestamp"]))

    def muestra_temas_fecha():
        # Dada una fecha por el usuario, mostrar toda la información de los temas
        # creados en esa fecha.
        # FIXME Esta opción no parece funcionar....
        print("Indique la fecha de los temas que quiere consultar (dd/mm/yyyy):")
        fecha_string = raw_input()
        try:
            fecha_datetime = datetime.datetime.strptime(fecha_string, "%d/%m/%Y")
        except ValueError:
            print("La fecha introducida no es correcta.")
            return

        qp = QueryParser("timestamp", index_temas.get_index().schema)
        with index_temas.get_index().searcher() as s:
            resultados = s.search(query.Every("timestamp"),
                                  filter=qp.parse("timestamp:"+fecha_datetime.strftime("%Y%m%d")))
            for resultado in resultados:
                print(u" - Título: " + resultado["title"])
                print(u"   Autor:  " + resultado["author"])
                print(u"   Enlace: " + resultado["link"])

    def muestra_temas_ordenados_por_visitas():
        # Mostrar los temas ordenados por número de visitas
        with index_temas.get_index().searcher() as s:
            temas = s.search(query.Every(), sortedby="visit_number")
            for tema in temas:
                print(u" - Título: " + tema["title"])
                print(u"   Visitas: " + str(tema["visit_number"]))

    def muestra_temas_de_autor():
        # Mostrar toda la información de los temas de un autor proporcionado por el usuario
        print("Introduce del nombre del autor:")
        autor = raw_input()

        qp = QueryParser("author", index_temas.get_index().schema)
        with index_temas.get_index().searcher() as s:
            temas = s.search(qp.parse(autor))
            mostrar_resultados(temas)

    def muestra_autor_mas_prolifico():
        # Mostrar el autor que más temas ha creado
        # TODO
        print("TODO")

    salir = False
    while not salir:
        opcion = muestra_menu(u"Menu Principal", (u"Buscar en título",                      # Opcion 1
                                                  u"Buscar en contenido de respuestas",     # Opcion 2
                                                  u"Mostrar temas de una fecha",            # Opcion 3
                                                  u"Mostrar temas ordenados por visitas",   # Opcion 4
                                                  u"Mostrar temas de un autor",             # Opcion 5
                                                  u"Mostrar autor mas prolífico",           # Opcion 6
                                                  u"Salir"))                                # Opcion 7

        if opcion == 1:
            busca_en_titulo()
        elif opcion == 2:
            busca_en_respuestas()
        elif opcion == 3:
            muestra_temas_fecha()
        elif opcion == 4:
            muestra_temas_ordenados_por_visitas()
        elif opcion == 5:
            muestra_temas_de_autor()
        elif opcion == 6:
            muestra_autor_mas_prolifico()
        elif opcion == 7:
            salir = True


if __name__ == "__main__":
    # Creamos o cargamos los indices
    buscador_temas = BuscadorTemasDerechoCivil("index_temas")
    buscador_respuestas = BuscadorRespuestasDerechoCivil("index_respuestas")

    if buscador_temas.is_empty() and buscador_respuestas.is_empty():
        # Si los indices estan vacíos, indexar
        print(u"Los índices son nuevos, indexando...")
        extraer_datos(buscador_temas, buscador_respuestas)
    else:
        # En otro caso solo informar de que los índices se han cargado
        print(u"No se realiza el indexado porque el índice existía previamente. Para reinicializar los índices debe "
              u"borrar las carpetas index_temas e index_respuestas.")

    # Interfaz de consola del programa
    menu_principal(buscador_temas, buscador_respuestas)
