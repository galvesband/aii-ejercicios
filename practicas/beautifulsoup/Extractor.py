# encoding=utf-8

import urllib2
from bs4 import BeautifulSoup

from Modelo import Modelo


def extraer_datos_cadena(modelo, url):
    assert isinstance(modelo, Modelo)

    html = urllib2.urlopen(url).read()
    soup = BeautifulSoup(html, "lxml")

    rset = []
    # Buscar tabla con categorias y demas
    my_font = soup.find("font", "TituloIndice")
    table_descendants = my_font.parent.parent.parent.descendants

    current_cat = "Desconocido"
    for my_tag in table_descendants:
        if my_tag.name == "font" and my_tag.attrs["class"][0] == "TituloIndice":
            current_cat = my_tag.string
        if my_tag.name == "a" and my_tag.attrs["class"][0] == "LinkIndice":
            current_url = my_tag.attrs["href"]
            current_title = my_tag.string
            rset.append((current_title, current_url, current_cat))

    modelo.insert_rows(rset)

    # devolvemos el html para almacenarlo anyway
    return html


if __name__ == "__main__":
    extraer_datos_cadena(Modelo("practica1.db"), "http://www.sevillaguia.com/sevillaguia/agendacultural"
                                                 "/agendacultural.asp")
