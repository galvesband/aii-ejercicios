# encoding=utf-8

import sqlite3


class Modelo:
    def __init__(self, path_to_db):
        self.path_to_db = path_to_db
        self.connection = sqlite3.connect(self.path_to_db)
        # Para evitar problemas con el conjunto de caracteres que maneja la BD
        self.connection.text_factory = str

    def __delete__(self, instance):
        if isinstance(self.connection, sqlite3.Connection):
            self.connection.close()

    def create_database(self):
        # Crear modelo de datos, eliminar el anterior si existe
        self.connection.execute('''
            DROP TABLE IF EXISTS CATEGORIAS;
        ''')
        self.connection.execute('''
            CREATE TABLE CATEGORIAS (
              ID INTEGER PRIMARY KEY AUTOINCREMENT,
              TITULO            TEXT NOT NULL,
              LINK              TEXT NOT NULL,
              CATEGORIA         TEXT NOT NULL
            );
        ''')

    def insert_row(self, title, link, categoria):
        print("Insertando en modelo la categoria " + title)
        self.connection.execute('''
            INSERT INTO CATEGORIAS (TITULO, LINK, CATEGORIA)
            VALUES (?, ?, ?);
        ''', (title, link, categoria))

    def commit(self):
        self.connection.commit()

    def insert_rows(self, lista):
        for record in lista:
            self.insert_row(record[0], record[1], record[2])
        self.connection.commit()

    def search(self, search_term):
        rset = self.connection.execute('''
            SELECT TITULO, LINK FROM CATEGORIAS c
            WHERE c.TITULO like ?
        ''', ("%" + search_term + "%",))

        return rset

    def get_all(self):
        rset = self.connection.execute('''
            SELECT TITULO, LINK FROM CATEGORIAS
        ''')

        return rset
