# encoding=utf-8
from Tkinter import *
from Modelo import Modelo
from Extractor import extraer_datos_cadena


def ventana_principal():
    def extraer_datos_tk():
        extraer_datos_cadena(modelo, 'http://www.sevillaguia.com/sevillaguia/agendacultural/agendacultural.asp')

    def buscar_bd_tk():
        def listar_busqueda_tk(event):
            print("Buscando por: " + en_search.get())
            cursor = modelo.search(en_search.get())
            mostrar_etiquetas(cursor)

        ventana_busqueda = Toplevel()
        lbl = Label(ventana_busqueda, text="Introduzca una cadena: ")
        lbl.pack()
        en_search = Entry(ventana_busqueda)
        en_search.bind("<Return>", listar_busqueda_tk)
        en_search.pack()

    def mostrar_todas_tk():
        cursor = modelo.get_all()
        mostrar_etiquetas(cursor)

    modelo = Modelo("practica1.db")
    modelo.create_database()

    top = Tk()
    almacenar = Button(top, text="Almacenar", command=extraer_datos_tk)
    almacenar.pack(side=LEFT)

    buscar = Button(top, text="Buscar", command=buscar_bd_tk)
    buscar.pack(side=LEFT)

    mostrar_todas = Button(top, text="Mostrar todas", command=mostrar_todas_tk)
    mostrar_todas.pack(side=LEFT)

    top.mainloop()


def mostrar_etiquetas(lista):
    ventana = Toplevel()
    barra_scroll = Scrollbar(ventana)
    barra_scroll.pack(side=RIGHT, fill=Y)
    lb = Listbox(ventana, width=150, yscrollcommand=barra_scroll.get)
    for elemento in lista:
        lb.insert(END, elemento[0])
        lb.insert(END, elemento[1])
        lb.insert(END, '')
    lb.pack(side=LEFT, fill=BOTH)
    barra_scroll.config(command=lb.yview)


if __name__ == "__main__":
    ventana_principal()
