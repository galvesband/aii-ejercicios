# encoding=utf-8

import sqlite3
import urllib2
import re
from bs4 import BeautifulSoup


class Modelo:
    def __init__(self, path_to_db):
        self.path_to_db = path_to_db
        self.connection = sqlite3.connect(self.path_to_db)
        self.connection.text_factory = str

    def create_database(self):
        # Crear modelo de datos, eliminar el anterior si existe
        self.connection.execute('''
            DROP TABLE IF EXISTS RESULTADOS;
        ''')
        self.connection.execute('''
            CREATE TABLE RESULTADOS (
                ID INTEGER PRIMARY KEY AUTOINCREMENT,
                JORNADA           TINYINT NOT NULL,
                LINK              TEXT NOT NULL,
                EQUIPO1           TEXT NOT NULL,
                EQUIPO2           TEXT NOT NULL,
                RESULTADO         TEXT NOT NULL
            );
        ''')
        self.connection.commit()

    def insert_rows(self, lista_resultados):
        for resultado in lista_resultados:
            self.connection.execute('''
                INSERT INTO RESULTADOS (JORNADA, LINK, EQUIPO1, EQUIPO2, RESULTADO)
                VALUES (?, ?, ?, ?, ?);
            ''', resultado)
        self.connection.commit()

    def get_results_round(self, jornada):
        rset = self.connection.execute('''
            SELECT ID, JORNADA, LINK, EQUIPO1, EQUIPO2, RESULTADO FROM RESULTADOS
            WHERE JORNADA = ?;
        ''', (jornada,))

        return rset

    def get_results_team(self, team):
        rset = self.connection.execute('''
            SELECT ID, JORNADA, LINK, EQUIPO1, EQUIPO2, RESULTADO FROM RESULTADOS
            WHERE EQUIPO1 = ? OR EQUIPO2 = ?;
        ''', (team, team))

        return rset

    def get_rounds(self):
        rset = self.connection.execute('''
            SELECT DISTINCT(JORNADA) FROM RESULTADOS;
        ''')

        return rset


# Extrae jornadas y resultados y lo guarda en BD
def extract_data(modelo, url):
    assert isinstance(modelo, Modelo)

    # Cargamos html en Beautiful Soup
    html = urllib2.urlopen(url).read()
    soup = BeautifulSoup(html, "lxml")

    # Creamos base de datos
    modelo.create_database()

    # Aqui guardaremos los resultados que queremos insertar
    rset = []
    # Buscamos resultados de las jornadas listadas en la web
    # Primero localizamos las jornadas
    id_regexp = r"^jornada-([0-9])+$"
    jornadas_divs = soup.find_all("div", {"id": re.compile(id_regexp)})
    print("Encontradas " + str(len(jornadas_divs)) + " jornadas.")
    for jornada_div in jornadas_divs:
        # Determinar la jornada que estamos examinando
        title_a = jornada_div.find("a")
        jornada = int(str(title_a.string).replace("Jornada ", ""))

        # Encontrar resultados de la jornada e ir almacenandolos en rset
        partido_trs = jornada_div.find_all("tr", {"itemtype": "http://schema.org/SportsEvent"})
        # Para cada partido tenemos que determinar equipo1, equipo2, resultado y enlace a retransmisión
        for partido_tr in partido_trs:
            # Equipos
            equipos_span = partido_tr.find_all("span", "nombre-equipo")
            equipo1 = equipos_span[0].string
            equipo2 = equipos_span[1].string
            # Resultado y enlace
            resultado_a = partido_tr.find("a", "resultado")
            if resultado_a:
                enlace = resultado_a.attrs["href"]
                resultado = str(resultado_a.string).strip()

                # Solo queremos resultados de partidos terminados o en juego
                regexp = re.compile("^\s*[0-9]+ - [0-9]+\s*$")
                if regexp.match(resultado):
                    # Añadir resultado a rset
                    rset.append((jornada, enlace, equipo1, equipo2, resultado))

    # Añadir resultados a bd
    print("Insertando " + str(len(rset)) + " resultados.")
    modelo.insert_rows(rset)


def get_goles(url):
    def get_goles_de_eventos(eventos):
        rset_2 = []
        for evento in eventos:
            sub_eventos = evento.find_all("div", {"class": "txt-evento"})
            for sub_evento in sub_eventos:
                es_gol = sub_evento.find("span", {"class": "as-icon-futbol"})
                if es_gol:
                    minuto_str = sub_evento.find("span", {"class": "min-evento"})
                    minuto = int(re.search("\d+", minuto_str.string).group(0))
                    jugador = sub_evento.find("strong").string
                    rset_2.append((minuto, jugador))

        return rset_2

    html = urllib2.urlopen(url).read()
    soup = BeautifulSoup(html, "lxml")

    eventos_local = soup.find("div", {"class": "eventos-local"})
    rset = get_goles_de_eventos(eventos_local.find_all("div", {"class": "evento"}))
    eventos_visitante = soup.find("div", {"class": "eventos-visit"})
    rset += get_goles_de_eventos(eventos_visitante.find_all("div", {"class": "evento"}))

    # return rset
    rset.sort(key=lambda resultado: resultado[0])
    return rset


if __name__ == "__main__":
    # m = Modelo("resultados.db")
    # extract_data(m, "http://resultados.as.com/resultados/futbol/primera/2016_2017/calendario/")
    # for mi_jornada in m.get_rounds():
    #     print(str(mi_jornada[0]))

    url_goles_jornada1_rsociedad_madrid = "http://resultados.as.com/resultados/futbol/primera/2016_2017/directo" \
                                          "/regular_a_1_179516/ "
    for gol in get_goles(url_goles_jornada1_rsociedad_madrid):
        print(gol)
