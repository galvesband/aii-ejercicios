# encoding=utf-8
from Tkinter import *
from Modelo import Modelo, extract_data, get_goles


def ventana_principal():
    def extract_tk():
        extract_data(modelo, "http://resultados.as.com/resultados/futbol/primera/2016_2017/calendario/")

    def search_goals_tk():
        # Al seleccionar una jornada en la lista izquierda
        def on_round_selected(evt):
            if len(jornadas_lst.curselection()) > 0:
                jornada_seleccionada = dict_jornadas[jornadas_lst.curselection()[0]]
                print("Jornada seleccionada: " + str(jornada_seleccionada[0]))

                # limpiar lsita de partidos
                partidos_lst.delete(0, END)
                dict_partidos.clear()
                # insertar los partidos guardándolos en un diccionario
                i2 = 0
                for match in modelo.get_results_round(jornada_seleccionada[0]):
                    dict_partidos[i2] = match
                    partidos_lst.insert(i2, match[3] + " " + match[5] + " " + match[4])
                    i2 += 1

        # Al seleccionar un partido
        def on_match_selected(evt):
            if (len(partidos_lst.curselection())) > 0:
                index_partido_seleccionado = partidos_lst.curselection()[0]
                match_selected = dict_partidos[index_partido_seleccionado]
                print("Partido seleccionado: " + match_selected[3] + " " + match_selected[5] + " " + match_selected[4])

                goles = get_goles("http://resultados.as.com" + match_selected[2])
                show_goals(match_selected, goles)

        # Muestra goles de una jornada
        def show_goals(match, goals):
            ventana_goles = Toplevel()
            goles_lbl = Label(ventana_goles, text="Goles del partido " + match[3] + " " + match[5] + " " + match[4])
            goles_lbl.pack(side=TOP)

            goles_lst = Listbox(ventana_goles)
            i = 0
            for gol in goals:
                goles_lst.insert(i, str(gol[0]) + ": " + gol[1])
                i += 1
            goles_lst.pack(side=LEFT)

        # Ventana de búsqueda
        # Un label arriba con info y dos listboxs debajo para jornadas y partidos
        ventana_busqueda = Toplevel()
        info_lbl = Label(ventana_busqueda, text="Seleccione una jornada y un partido: ")
        info_lbl.pack(side=TOP)

        # Lista de jornadas
        jornadas_lst = Listbox(ventana_busqueda, selectmode="SINGLE")
        i = 0
        dict_jornadas = {}
        for jornada in modelo.get_rounds():
            dict_jornadas[i] = (jornada[0],)
            jornadas_lst.insert(i, "Jornada " + str(jornada[0]))
            i += 1
        jornadas_lst.bind("<<ListboxSelect>>", on_round_selected)
        jornadas_lst.pack(side=LEFT)

        # Lista de partidos
        partidos_lst = Listbox(ventana_busqueda, selectmode="SINGLE")
        partidos_lst.bind("<<ListboxSelect>>", on_match_selected)
        dict_partidos = {}
        partidos_lst.pack(side=LEFT)

    modelo = Modelo("resultados.db")
    modelo.create_database()

    # Ventana principal
    # Dos botones, uno para leer html y extraer jornadas y resultados,
    # el otro para mostrar ventana de búsqueda.
    top = Tk()
    almacenar_btn = Button(top, text="Almacenar resultados", command=extract_tk)
    almacenar_btn.pack(side=LEFT)

    buscar_goles_btn = Button(top, text="Buscar goles", command=search_goals_tk)
    buscar_goles_btn.pack(side=LEFT)

    top.mainloop()

if __name__ == "__main__":
    ventana_principal()
