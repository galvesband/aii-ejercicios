# encoding=utf-8

import urllib
import urllib2
from bs4 import BeautifulSoup
import re
from whoosh.index import create_in, open_dir
from whoosh.fields import *
from whoosh.qparser import QueryParser
import os.path
import datetime


class BuscadorTemasDerechoCivil:
    def __init__(self, path_to_index):
        if not os.path.exists(path_to_index):
            os.mkdir(path_to_index)
            # Crear schema e índice
            schema = Schema(
                title=TEXT(stored=True),
                link=ID(stored=True),
                author=TEXT(stored=True),
                answer_number=NUMERIC(stored=True),
                visit_number=NUMERIC(stored=True),
                content=TEXT,
            )
            self.index = create_in(path_to_index, schema)
            self.index_empty = True
        else:
            self.index = open_dir(path_to_index)
            self.index_empty = False

    def is_empty(self):
        return self.index_empty

    def get_index(self):
        return self.index


class BuscadorRespuestasDerechoCivil:
    def __init__(self, path_to_index):
        if not os.path.exists(path_to_index):
            os.mkdir(path_to_index)
            # Crear schema e índice
            schema = Schema(
                thread_link=ID(stored=True),
                author=TEXT(stored=True),
                timestamp=DATETIME(stored=True),
                content=TEXT
            )
            self.index = create_in(path_to_index, schema)
            self.index_empty = True
        else:
            self.index = open_dir(path_to_index)
            self.index_empty = False

    def is_empty(self):
        return self.index_empty

    def get_index(self):
        return self.index


def extraer_datos(index_temas, index_respuestas):
    def extraer_num_paginas(soup):
        # Obtenemos el número de páginas del foro
        pagination_form = soup.find("form", {"class": "pagination"})
        paginas_a = pagination_form.find("a", {"href": "javascript://", "class": "popupctrl"})
        return int(paginas_a.string.replace(u"Página 1 de ", u""))

    def extraer_temas_y_respuestas(soup):
        writer_temas = index_temas.get_index().writer()
        writer_respuestas = index_respuestas.get_index().writer()

        hilos_lis = soup.findAll("li", {"class": "threadbit"})
        for hilo_li in hilos_lis:
            # De cada hilo queremos el título, el enlace, el autor, la fecha,
            # el número de respuestas y visitas y el contenido

            # Primero el título y el enlace
            title_a = hilo_li.find("a", {"class": "title"})
            titulo = title_a.string
            link = title_a.attrs["href"]

            # Ahora el autor y la fecha
            autor = hilo_li.find("a", {"class": "username"}).string
            fecha_posible_dds = hilo_li.findAll("dd")
            fecha = False
            for posible_fecha_dd in fecha_posible_dds:
                if posible_fecha_dd.find("span", {"class": "time"}):
                    fecha = str(posible_fecha_dd.contents[0]).replace(",", "")
                    break
            if not fecha:
                fecha = "<fecha desconocida>"

            # Número de respuestas y visitas
            estadisticas_hilo_ul = hilo_li.find("ul", {"class": "threadstats"})
            respuestas, visitas = 0, 0
            for child in estadisticas_hilo_ul.children:
                if child.name == "li" and "class" not in child.attrs:
                    li = child

                    if li.find("a", {"class": "understate"}):
                        respuestas = int(str(li.find("a", {"class": "understate"}).string).replace(",", ""))

                    results = re.findall(r"^Visitas:\s+((\d|,)+)$", str(li.string).strip())
                    if len(results) > 0:
                        visitas = int(str(results[0][0]).replace(',', ''))

            # Necesitamos el contenido y parsear las respuestas al tema
            url_detalle_tema = ("http://foros.derecho.com/" + link).encode("UTF-8")
            print("Analizando tema: " + url_detalle_tema)
            soup_detalle_tema = BeautifulSoup(urllib2.urlopen(url_detalle_tema).read(), "lxml")

            # Contenido
            contenido = extraer_contenido_tema(soup_detalle_tema)

            # Añadir al indice de temas (cuando tenga el contenido)
            writer_temas.add_document(
                title=unicode(titulo),
                link=unicode(link),
                author=unicode(autor),
                answer_number=respuestas,
                visit_number=visitas,
                content=unicode(contenido)
            )

            # Extraer e indexar respuestas al tema
            extraer_respuestas(soup_detalle_tema, writer_respuestas, link)

        writer_temas.commit()
        writer_respuestas.commit()

    def extraer_contenido_tema(soup):
        posts_ol = soup.find("ol", {"id": "posts"})
        blockquote = posts_ol.find("blockquote")

        return unicode(blockquote.text)

    def extraer_respuestas(soup, writer, link):
        # Queremos indexar el enlace del tema, el autor, la fecha y el contenido
        # de cada respuesta. El enlace nos lo dan como argumento y es lo que
        # enlaza ambos índices.
        post_lis = soup.find("ol", {"class": "posts"}).find_all("li", {"class": "postcontainer"})
        primero = True
        for post_li in post_lis:
            if primero:
                # Ignoramos el primer post, que es el post original y esta
                # indexado en el indice de temas.
                primero = False
                continue
            else:
                # Nombre de usuario
                username = post_li.find("a", {"class": "username"}).find("strong").string
                # Fecha
                date_string = post_li.find("span", {"class": "date"}).text
                # Contenido
                content = post_li.find("blockquote", {"class": "postcontent"}).text

                # Escribimos en el índice
                writer.add_document(
                    thread_link=unicode(link),
                    author=unicode(username),
                    timestamp=datetime.datetime.strptime(unicode(date_string).replace(u'\xa0', u' '), u"%d/%m/%Y, %H:%M"),
                    content=unicode(content)
                )

    url_principal_temas = "http://foros.derecho.com/foro/20-Derecho-Civil-General/page{page_number}"

    # Descargamos la primera página del foro
    print("Analizando primera página del foro: " + url_principal_temas)
    html_pagina_1 = urllib2.urlopen(url_principal_temas.replace("{page_number}", "1")).read()
    soup_pagina_1 = BeautifulSoup(html_pagina_1, "lxml")
    # Obtenemos el número de páginas del foro
    num_paginas = extraer_num_paginas(soup_pagina_1)
    # FIXME Limitar artificialmente número de páginas parseadas
    num_paginas = 1
    print(u"Número de páginas del foro: " + str(num_paginas))

    # Extraemos temas y respuestas de la página
    extraer_temas_y_respuestas(soup_pagina_1)

    # Extraer temas y respuestas de las otras páginas
    if num_paginas > 1:
        for i in range(2, num_paginas):
            # URL de página i
            url_pagina_i = url_principal_temas.replace("{page_number", str(i))
            # HTML de página i
            html_pagina_i = urllib2.urlopen(url_pagina_i).read()
            # Sopa de página i
            soup_pagina_i = BeautifulSoup(html_pagina_i, "lxml")

            extraer_temas_y_respuestas(soup_pagina_i)


if __name__ == "__main__":
    buscador_temas = BuscadorTemasDerechoCivil("index_temas")
    buscador_respuestas = BuscadorRespuestasDerechoCivil("index_respuestas")

    if buscador_temas.is_empty() and buscador_respuestas.is_empty():
        print(u"Los índices son nuevos, indexando...")
        extraer_datos(buscador_temas, buscador_respuestas)
    else:
        print(u"No se realiza el indexado porque el índice existía previamente.")
