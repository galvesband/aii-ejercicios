# coding=utf-8
from __future__ import absolute_import
import unittest
from ejercicios.busqueda.Ejercicio1 import ejercicio1


class TestEjerciciosBusqueda(unittest.TestCase):
    def test_ejercicio1(self):
        lista = [("Anton Banksy", "123456"), ("Sasha Putin", "987654"), ("Antonio Lirondo", "506978")]
        expected = [("Anton Banksy", "123456"), ("Antonio Lirondo", "506978")]
        self.assertEqual(expected, ejercicio1("Anton", lista))

if __name__ == '__main__':
    unittest.main()
