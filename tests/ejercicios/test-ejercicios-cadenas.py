# coding=utf-8
from __future__ import absolute_import
import unittest
from ejercicios.cadenas.Ejercicio1 import Ejercicio1
from ejercicios.cadenas.Ejercicio2 import Ejercicio2


class TestEjerciciosCadena(unittest.TestCase):
    def test_ejercicio1a(self):
        ejercicio1 = Ejercicio1()
        self.assertEqual("c,a,d,e,n,a", ejercicio1.ejercicio1a("cadena", ','))

    def test_ejercicio1b(self):
        ejercicio1 = Ejercicio1()
        self.assertEqual("1:::234:56", ejercicio1.ejercicio1b("1___234_56", ':'))

    def test_ejercicio1c(self):
        ejercicio1 = Ejercicio1()
        self.assertEqual("Su clave es: ****", ejercicio1.ejercicio1c("Su clave es: 1234", '*'))

    def test_ejercicio1d(self):
        ejercicio1 = Ejercicio1()
        self.assertEqual("255.255.255.0", ejercicio1.ejercicio1d("2552552550", '.'))

    def test_ejercicio2a(self):
        ejercicio2 = Ejercicio2()
        self.assertEqual(True, ejercicio2.ejercicio2a("cadena", "micadena"))
        self.assertEqual(False, ejercicio2.ejercicio2a("no", "es una subcadena"))

    def test_ejercicio2b(self):
        ejercicio2 = Ejercicio2()
        self.assertEqual("gnome", ejercicio2.ejercicio2b("kde", "gnome"))
        self.assertEqual("gnome", ejercicio2.ejercicio2b("gnome", "kde"))

if __name__ == '__main__':
    unittest.main()
