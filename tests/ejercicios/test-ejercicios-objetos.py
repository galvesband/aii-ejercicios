# coding=utf-8
from __future__ import absolute_import
import unittest
from ejercicios.objetos.Corcho import Corcho
from ejercicios.objetos.Botella import Botella
from ejercicios.objetos.Sacacorchos import Sacacorchos


class TestObjetos(unittest.TestCase):
    def test_something(self):
        corcho = Corcho("Vivanco")
        botella = Botella(corcho)
        sacacorchos = Sacacorchos()
        self.assertEqual("Vivanco", corcho.nombre_bodega)
        self.assertEqual(corcho, botella.corcho)
        self.assertEqual(None, sacacorchos.corcho)

        sacacorchos.destapar(botella)
        self.assertEqual(None, botella.corcho)
        self.assertEqual(corcho, sacacorchos.corcho)

        sacacorchos.limpiar()
        self.assertEqual(None, sacacorchos.corcho)

if __name__ == '__main__':
    unittest.main()
