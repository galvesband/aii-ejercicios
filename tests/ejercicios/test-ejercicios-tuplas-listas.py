# coding=utf-8
from __future__ import absolute_import
import unittest
from ejercicios.tuplas_listas.Ejercicio1 import Ejercicio1
from ejercicios.tuplas_listas.Ejercicio2 import ejercicio2


class MyTestCase(unittest.TestCase):
    def test_ejercicio1a(self):
        print(" *** ejercicio1a:")
        ejercicio1 = Ejercicio1()
        # Esta función no devuelve nada. Me conformo con ejecutarla.
        ejercicio1.ejercicio1a(("Rafa", "Pedro"))

    def test_ejercicio1b(self):
        print(" *** ejercicio1b:")
        ejercicio1 = Ejercicio1()
        tuplas = "Rafa", "Pedro", "Antonio", "Fernando", "Juan", "Batman"
        ejercicio1.ejercicio1b(tuplas, 2, 2)

    def test_ejercicio1c(self):
        print(" *** ejercicio1c: (A)")
        ejercicio1 = Ejercicio1()
        nombres_y_generos = ("Rafa", 'm'), ("María", 'f'), ("Pedro", 'm'), ("Julia", 'f'), ("Antonio", 'm'), ("Juan", 'm')
        ejercicio1.ejercicio1c1a(nombres_y_generos)
        print(" *** ejercicio1c: (B)")
        ejercicio1.ejercicio1c1b(nombres_y_generos, 2, 2)

    def test_ejercicio2(self):
        print(" *** ejercicio2:")
        tuplas = []
        expected = []
        tuplas.append(("Rafa", "J", "Simpson"))
        expected.append("Rafa J. Simpson")

        tuplas.append(("Jordi", "J", "Varas"))
        expected.append("Jordi J. Varas")

        tuplas.append(("María", "D", "Dolores"))
        expected.append("María D. Dolores")

        self.assertEqual(expected, ejercicio2(tuplas))


if __name__ == '__main__':
    unittest.main()
